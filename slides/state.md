# State management

---slide---

## What is state?
Values within your application

---slide---

## Added complexity

<img style="float: left; height: 300px;  transform: scaleX(-1);" src="slides/images/redcar.png">
<img style="float: right; height: 300px;" src="slides/images/bluecar.png">


---slide---

## OO Car

<pre><code class="hljs java" style="max-height: 100%;font-size:200%;padding: 0.5em">public class Car {
  
  
  private String colour;
  


  public String getColour() {
  
  
    return colour;
	
	
  }


  public void setColour(String colour) {
  
  
    this.colour = colour;
	
	
  }
  
}
</code></pre>

---slide---

## FP Car

<pre><code class="hljs java" style="max-height: 100%;font-size:200%;padding: 0.5em">public class Car {


  private final String colour;
  
  

  public Car(String colour) {
  
  
    this.colour = colour;
	
	
  }
  
}
</code></pre>

---slide---

## Time

> value = (state, time)

---slide---

## Problems with state

* Complex
	* Race conditions
	* Concurrency
* High cognitive load

---slide---

## Cognitive load

<pre><code class="hljs java" style="max-height: 100%;font-size:200%;padding: 0.5em">public void foo(Car redCar) {


  System.out.println(redCar);
  
  
  paint(redCar);
  
  
  System.out.println(redCar);
  
  
}
</code></pre>

---slide---

## Immutable objects

<pre><code class="hljs java" style="max-height: 100%;font-size:200%;padding: 0.5em">//Java 17

public record ImmutableCar(String colour, String brand){}
</code></pre>

---slide---

### Cognitive load

<pre><code class="hljs java" style="max-height: 100%;font-size:200%;padding: 0.5em">public void foo(ImmutableCar redCar) {


  System.out.println(redCar);
  
  
  paint(redCar);
  
  
  System.out.println(redCar);
  
  
}
</code></pre>

---slide---

### Changing values

<pre><code class="hljs java" style="max-height: 100%;font-size:200%;padding: 0.5em">public ImmutableCar paintBlue(ImmutableCar car) {


  return new ImmutableCar("Blue", car.getBrand());
  
  
}
</code></pre>
		
---slide---

### Explicit state change

<pre><code class="hljs java" style="max-height: 100%;font-size:200%;padding: 0.5em">public void foo(ImmutableCar redCar) {


  System.out.println(redCar);
  
  
  ImmutableCar blueCar = paintBlue(redCar);
  
  
  System.out.println(blueCar);
  
  
}
</code></pre>

---slide---
	
## Tradeoffs

* Consistency vs Performance
	
---slide---
	
### Performance
	
---slide---
	
### Dealing with tradeoffs	

* Function signatures should be immutable
* Keep mutable state function scoped

<pre><code class="hljs java" style="max-height: 100%;font-size:200%;padding: 0.5em">public String foo(String input) {

  var builder = new StringBuilder(input);
  
  
  for(var i = 0; i < 1_000_000; i++) {
  
  
    builder.append("0");
	
	
  }
  
  
  return builder.toString();
  
}
</code></pre>

---slide---

## Making illegal states unrepresentable

* Making sure the content of the state is correct


---slide---

## Nullable fields

---slide---

### Optional fields

<pre><code class="hljs java" style="max-height: 100%;font-size:200%;padding: 0.5em">public record Person(


  String firstName,
  
  
  @Nullable String middleName,
  
  
  String lastName
  
){}
</code></pre>

---slide---

### Optional fields?

<pre><code class="hljs java" style="max-height: 100%;font-size:200%;padding: 0.5em">public record Contract(


  String text,
  
  
  @Nullable String signature
  
  
){}
</code></pre>

---slide---

### Effect

<pre><code class="hljs java" style="max-height: 100%;font-size:200%;padding: 0.5em">public void startProject(Contract contract) {


  if(contract.signature() == null){
  
  
    throw new IllegalStateException();
	
	
  }
  
}
</code></pre>

---slide---

### State changes as types

<pre><code class="hljs java" style="max-height: 100%;font-size:200%;padding: 0.5em">public record Contract(


  String text
  
  
){}


public record SignedContract(


  String text,
  
  
  String signature
  
  
){}
</code></pre>

---slide---

### Types as architecture

<pre><code class="hljs java" style="max-height: 100%;font-size:200%;padding: 0.5em">public void startProject(SignedContract contract) {


  // No null check needed
  
  
}
</code></pre>

---slide---

## Key take-aways

* State is difficult
* Mutability adds the complexity of time
* Mutable state is not inherently bad
* We can use types to remove null values
