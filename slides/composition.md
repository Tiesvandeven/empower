# Composition

---slide---

## Function Signatures

<pre>
<code class="hljs java" style="max-height: 200%;font-size:200%;padding: 0.5em">public String getName(Person input) { ... } // Person -> String


public int length(String input) { ... } // String -> Int


length(getName(input)) // Person -> Int
</code>
</pre>

---slide---

### Domino

<img style="width:1000px;" src="slides/images/domino.jpg">

---slide---

### In code

> Given a list of Person's sum the total age of everyone with a name starting with "A"

---slide---

### Using for

<pre><code class="hljs java" style="max-height: 100%;font-size:200%;padding: 0.5em">var totalLength = 0;


var persons = List.of(new Person("A", 11), new Person("B",20));


for(Person p : persons) {
	
	
  if(p.getName().startsWith("A")) {
	  
	  
    totalLength += p.getAge();
		
		
  }
	
	
}
</code></pre>

---slide---

### Breaking down the problem

> Given a list of Person's sum the total age of everyone with a name starting with "A"

* Only select people with a name starting with "A"
* Get the age of the person
* Sum all ages

---slide---

### Using streams

<pre><code class="hljs java" style="max-height: 100%;font-size:200%;padding: 0.5em">public boolean startsWithA(Person p) {

  return p.getName().startsWith("A");

}

var persons = List.of(new Person("A", 11), new Person("B",20));


var totalLength = persons.stream()

          .filter(p -> startsWithA(p))
		  
          .mapToInt(p -> p.getAge())
		  
          .sum();
</code></pre>

---slide---

### Bash

> ls | grep a

---slide---

## Composable Problem solving

* Programming is problem solving
* Problems can be divided

---slide---

<img style="width:1000px;" src="slides/images/design.svg">
		
---slide---

<img style="width:1500px;" src="slides/images/design2.svg">
		
---slide---

<img style="width:1500px;" src="slides/images/design3.svg">

---slide---

## Key take-aways

* Problems are compositions of sub-problems
* Consider streaming API
