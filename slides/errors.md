# Error handling

---slide---

## Things will fail

* Dropped connections
* Overflow

---slide---

## Error types

* Recoverable errors
  * Using a default value
  * Use an alternative code path
  * Return a different status code
* Unrecoverable (Technical errors)
  * Usually returns status code 500 

---slide---

## Default approaches

* Hope
* Runtime Exceptions
* Returning null

---slide---

## Runtime Exceptions

<pre><code class="hljs java" style="max-height: 100%;font-size:200%;padding: 0.5em">/**

* Will throw an ArithmeticException if b equals 0

*/

public double divide(double a, double b) {


  if (b == 0) {
  
  
    throw new ArithmeticException();
	
	
  }
  
  
  return a / b;


}
</code></pre>

---slide---

## Returning null

<pre><code class="hljs java" style="max-height: 100%;font-size:200%;padding: 0.5em">/**

* Will return null if b equals 0

*/

public Double divide(double a, double b) {


  if (b == 0) {
  
  
    return null;
	
	
  }
  
  
  return a / b;


}
</code></pre>

---slide---

## Other options

* Checked Exceptions
* Optional
* Sealed types

---slide---

### Checked exceptions

<pre><code class="hljs java" style="max-height: 100%;font-size:200%;padding: 0.5em">public String doSomething() throws FileNotFoundException {


  ...
  
  
}
</code></pre>

---slide---

### Checked exceptions

* Showing the method can fail
  * Function has multiple return types
* Offering a higher context to recover
* Errors must be reasoned about
* Errors are type checked
* Great for recoverable errors

---slide---			
		
### Optional

<pre><code class="hljs java" style="max-height: 100%;font-size:200%;padding: 0.5em">// Java 8


public Optional&lt;Double> divide(double a, double b) {


  if (b == 0) {
  
  
    return Optional.empty();
	
	
  }
  
  
  return Optional.of(a / b);
  
}
</code></pre>

---slide---

#### Optional

* Recoverable
  * orElse() / orElseGet()
* Composable
  * map() / flatMap()
* No error information

---slide---

### Modelling error state as a type

* Just like SignedContract
* Function return type is a
  * Success
  * Failure

---slide---

### Sealed types

* Combines checked exceptions and Optional

<pre><code class="hljs java" style="max-height: 100%;font-size:200%;padding: 0.5em">//Java 19


public sealed interface Result {


  record Success(int value) implements Result {}


  record Failure(String error) implements Result{}
  
}
</code></pre>

---slide---

#### Example

<pre><code class="hljs java" style="max-height: 100%;font-size:200%;padding: 0.5em">public Result divide(int a, int b) {


  if (b == 0) {
  
  
    return new Result.Failure("b should not be 0");
	
	
  }
  
  
  return new Result.Success(a / b);
  
}
</code></pre>
	
---slide---

#### Pattern matching

<pre><code class="hljs java" style="max-height: 100%;font-size:200%;padding: 0.5em">//Java 21


public void handle(Result input) {


  var output = switch (input) {
  
  
    case Result.Success s -> "The value is:" + s.value());
	
	
    case Result.Failure f -> "The error is:" + f.error());
	
	
  }
  
  
  System.out.println(output);
  
}
</code></pre>

---slide---

## Key take-aways

* Think about error paths
* Model them if they are recoverable
* Runtime exceptions are not the only option
