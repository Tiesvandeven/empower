# Conclusion

* Composition can help with problem solving
* Be careful with state, consider immutability
* Don't trust the happy path, consider other options than runtime exceptions
* Be aware of your side-effects and choose where they taken place
* Be pragmatic not dogmatic
* You might already be a functional programmer

---slide---

## Keep in touch

<img style="float: right;" src="slides/images/ties.jpg">

* ties_ven @Twitter
* www.tiesvandeven.nl
* Or talk to me in real life
* Sheets: https://tiesvandeven.gitlab.io/empower/